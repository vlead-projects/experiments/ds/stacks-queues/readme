#+TITLE: STACKS AND QUEUES EXPERIMENT DASHBOARD
#+AUTHOR: VLEAD
#+DATE: [2018-05-16 Wed]
#+SETUPFILE: ../org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This virtual lab will give a basic introduction to stacks
  and queues. There will be a pre-test, interactive modules
  for stacks and queues operations followed by quizzes and
  thought problems.

* Hosted Link
  Hosted Link for the Stacks and Queues experiment :: [[http://exp-iiith.vlabs.ac.in/sq/exp.html][Stacks and Queues Link]]

* Scope of Project
  Students learning data structures would benefit from the
  virtual lab, since it involves interactive exercises,
  examples, and tests that would help a learner understand
  concepts and recall better.

  The project will follow pedagogy and will focus more on
  concepts, than the implementation or code.

* Project Developers and Contributors Details
  |------------------------+-----------------------+-------------+------------+------------------------------------+------------------|
  | *Names*                | *Year of Study*       | *Role*      | *Phone-No* | *Email-ID*                         | *gitlab handles* |
  |------------------------+-----------------------+-------------+------------+------------------------------------+------------------|
  | Mayank Taluj           | 2nd year Btech IIIT-H | Developer   | 9640079035 | mtaluja11@gmail.com                | mtaluja11        |
  |------------------------+-----------------------+-------------+------------+------------------------------------+------------------|
  | Sanjana Sunil          | 2nd year Btech IIIT-H | Developer   | 7893908982 | sanjana99sunil@students.iiit.ac.in | SanjanaSunil     |
  |------------------------+-----------------------+-------------+------------+------------------------------------+------------------|
  | Kalakonda Sai Shashank | 1st year Btech IIIT-H | Contributor | 9121485027 | sai.shashank@research.iiit.ac.in   | chinnu_sai25     |
  |------------------------+-----------------------+-------------+------------+------------------------------------+------------------|

* References
  A mapping between different documents and their roles.
  |--------+-------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
  | *S.NO* | *Link*                  | *Role*                                                                                                                                                                   |
  |--------+-------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
  |     1. | [[./structure.org][Structure]]               | Captures the structure of experiment content                                                                                                                             |
  |     2. | [[./design-spec.org][Design Spec]]             | Captures the design specs of the experiment structure and artefacts                                                                                                      |
  |     3. | [[https://gitlab.com/vlead-projects/experiments/ds/stacks-and-queues/content-html][Content]]                 | Captures the experiment content                                                                                                                                          |
  |     4. | [[https://gitlab.com/vlead-projects/experiments/ds/stacks-and-queues/artefacts][Artefacts]]               | This is where the artefacts of the experiments are implemented                                                                                                           |
  |     5. | [[https://docs.google.com/document/d/1DdRnoZguDg4jJkOk-D5MUDr7mCATExBVw6QSi1XioG0/edit][Deployment-Instructions]] | This document describes (build process and changes) reducing the manual work in each experiment deployment process and changes which we have made to reduce manual work. |
  |--------+-------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
  
* Assumptions
- Basic understanding of arrays and linked lists.
- A basic knowledge of time complexity notations.
- Some prior exposure to data strcutures.
- Understanding of English.

* StakeHolders
- College and School Students
- MHRD
- Teachers

* Value Added by our project
+ It would be beneficial for students of computer science.
+ Our experiment would benefit those who are interested in
  learning data structures for the first time by teaching
  fundamental data structures concepts.

* Risks and Challenges
+ We faced major issues in making responsive design.
+ Coding out in JavaScript to conform to the exact design we
  wanted wasn't very easy.
 
* User Feedback
+ This experiment has not been presented to the final user
  yet.
+ The outreach team will carry this forward, evaluate it and
  make required changes.

* Learnings and Experience
+ Got hands on experience with the full cycle of developing
  an app.
+ Understood the importance of proper documentation and
  readability.
+ Understood pedagogy and its importance.
+ It was a nice experience overall and we learnt a lot from
  this internship.
 
  

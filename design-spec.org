#+TITLE: DESIGN SPEC FOR STACKS AND QUEUES
#+AUTHOR: VLEAD
#+DATE: [2019-05-29 Wed]
#+SETUPFILE: org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: \n:t

This is the desgin document for Stacks and queues. This includes :
+ Mapping of experiment level learning objectives to Learning Units(LU).
+ Mapping of LU level learning objectives to tasks.
+ List of artefacts under each task.
+ Design specifications for each artefact.
+ Acceptance testing for artefacts.

* MAPPING OF LEARNING OBJECTIVES AT EXPERIMENT LEVEL :
| LEARNING OBJECTIVE | LEARNING UNIT | BLOOM'S TAXONOMY LEVEL |
|-------------------------------------------------------------|
|To recall basics of Arrays and Linked list| PRE-TEST | Remember and understand |
|To describe and explain about Stacks | STACKS |Remember and Understand |
|To describe and explain about Queues | QUEUES |Remember and Understand |
|To analyse the differences between stacks and queues | ANALYSIS | Analyze and Understand |
|To apply the concepts learnt and test understanding of user by attempting the post-test quiz | POST-TEST | Evaluate |

* MAPPING OF LEARNING OBJECTIVES AT LEARNING UNIT LEVEL

** LU 1	: PRE-TEST
| MODULE | TYPE OF MODULE | LU LEARNING OBJECTIVE | 
|-----+-----------------------|
|Recap1 (Arrays) | Task | To review the concepts of Arrays |
|Recap2 (Linked lists) | Task |To review the concepts of Linked lists |
|Pre-Test | Quiz |To test user's understanding of pre requisites |

** LU 2	: STACKS
| MODULE | TYPE OF MODULE | LU LEARNING OBJECTIVE |
|-----+-----------------------|
|Concept and Theory | Task | To explain the concept and theory of stacks. |
|Demo | Task | To show the working of basic operations of stacks |
|Practice : Arrays | Task | To show the implementation of stacks using arrays. |
|Practice : Linked lists | Task | To show the implementation of stacks using Linked lists |
|Exercise | Task | To test the understanding of user on basic operations of stacks. |
|Quiz on Stacks | Quiz | To test the understanding of user on concept of Stacks. |

** LU 3	: QUEUES
| MODULE | TYPE OF MODULE | LU LEARNING OBJECTIVE |
|-----+-----------------------|
|Concept and Theory | Task | To explain the concept and theory of queues. |
|Demo | Task | To show the working of basic operations of queues |
|Practice : Arrays | Task | To show the implementation of queues using arrays. |
|Practice : Linked lists | Task | To show the implementation of queues using Linked lists |
|Exercise | Task | To test the understanding of user on basic operations of queues. |
|Quiz on Queues | Quiz | To test the understanding of user on concept of queues. |

** LU 4 : ANALYSIS
| MODULE | TYPE OF MODULE | LU LEARNING OBJECTIVE |
|-----+-----------------------|
| Comparision of stacks and queues | Task | To compare Stacks and queues |
| Applications of stacks and queues | Task | General applications of stacks and queues |
| Analysis quiz | Quiz | To test the understanding of user .|

** LU 5 : POST-TEST 
| MODULE | TYPE OF MODULE | LU LEARNING OBJECTIVE |
|-----+-----------------------|
|Post-Test | Quiz | To test user's understanding on Stacks and queues. |

* LIST OF ARTEFACTS IN EACH TASK 

** LU 1 : PRE-TEST

| Module | ARTEFACT TYPE | DESIGN SPEC |
|------+---------------+-------------|
| Recap1 (Arrays) | Text and Images | The text should recap the concept of arrays. |
| Recap2 (Linked list) | Text and Images | The text should recap the concept of linked lists.|
| Pre-test quiz | MCQ | Each question tests the understanding of arrays and linked lists . |
 
** LU 2 : STACKS

| Module | ARTEFACT TYPE | DESIGN SPEC |
|------+---------------+-------------|
| Intro | Text and Images | Should explain the intuition of Stacks and a detailed introduction of Stacks . |
| Demo | Video | This video should explain the concept of Stacks and should give an experiment demo of Stacks and its basic operations with example |
| Demo | Interactive Element | IE provides an empty stack and a box to enter the element which user wants to add to the top of stack , which gets added on clicking =push= button and =pop= button pops out the top element|
| Practice (Arrays) |  Interactive element | IE provides an array of size 10 , a box to enter the elements of stack by user , a =PUSH= , =POP= and =CLEAR= buttons where on entering an element and clicking push button adds the entered element on the top of stack and pop button pops out the top element whereas clear button clears the whole array(i.e stack) |
| Practice (Linked list) |  Interactive element | IE provides a box to enter the value by user which gets added onto to the top of stack(represented as head) and pop button pops out the top element of stack (i.e current head element gets popped out) and next preceeding element of current head acts as new head of the linked list(top of stack) |
| Exercise | Interactive Element | IE provides a button =Generate a random question= which onclicking generates a random *stack A* and empty *stack B* where user is asked to pop the elements from stack in =ascending order= and to perform this user is provided with four buttons =pop from A and push to B= , =pop from B and push to A= , =pop from stack A= , =pop form stack B= and after popping out all the elements from both the stacks user needs to click =check answer= button which outputs whether the sequence selected by the user is in ascending order or not.|
| Quiz | MCQ | Each question tests the understanding of Stacks .|

** LU 3 : QUEUES

| Module | ARTEFACT TYPE | DESIGN SPEC |
|------+---------------+-------------|
| Intro | Text and Images | Should explain the intuition of Queues and a detailed introduction of Queues . |
| Demo | Video | This video should explain the concept of Queues and should give an experiment demo of Queues and its basic operations with example |
| Demo | Interactive Element | IE provides an empty queue and a box to enter the element which user wants to add to the front of queue(represented as tail) , which gets added on clicking =enqueue= button and =Dequeue= button pops out the head element|
| Practice (Arrays) |  Interactive element | IE provides an array of size 10 , a box to enter the elements of queue by user , an =ENQUEUE= and =DEQUEUE=  buttons where on entering an element and clicking enqueue button adds the entered element at the tail of array and dequeue button pops out the head element . |
| Practice (Linked list) |  Interactive element | IE provides a box to enter the value by user which gets added at the starting of queue (represented as head) onclicking =ENQUEUE= button and =DEQUEUE= button pops out the last element of queue |
| Exercise | Interactive Element | IE provides a button =Generate a random question= which onclicking generates a random sequence of numbers and a random queue , and user is asked to update the queue using =ENQUEUE= and =DEQUEUE= buttons so that the elements in queue should be in the same sequence of numbers as given in the question and when user thinks that updating the queue is done then user can click =check answer= button to know whetherr the update of sequence is performed right or wrong.|
| Quiz | MCQ | Each question tests the understanding of Queues .|

** LU 4 : ANALYSIS
| Module | ARTEFACT TYPE | DESIGN SPEC |
|------+---------------+-------------|
| Concept | Text and Images | Text should discuss differences between Stacks and Queues. |
| Comparision | Video | Video should explain the differences between Stacks and Queues with example .|
| Quiz | MCQ | Each question tests the understanding on differences between stacks and queues .|

** LU 5 : POST-TEST 
| Module | ARTEFACT TYPE | DESIGN SPEC |
|------+---------------+-------------|
| Quiz | MCQ | Each question tests the understanding of Stacks and Queues . |

* Acceptance Testing

** Acceptance Testing for Video Artefacts

+ Given video, when I load page, video loads
+ Given video, when I press play button, video plays
+ Given video, when I press pause button, video stops

** Acceptance testing for Interactive Elements

*** Demo artefact for Stacks

+ Given artefact, consists an empty stack and buttons which perform basic operations on stacks.

*** Demo artefact for Queues

+ Given artefact, consists an empty queue and buttons which perform basic operations on queues.

*** Practice artefacts for stacks

+ Given artefact either consists an array or linked list and user is asked to implement the basic operations of stacks using the buttons provided.

*** Practice artefacts for queues

+ Given artefact either consists an array or linked list and user is asked to implement the basic operations of queues using the buttons provided.

*** Excercise artefacts

+ Given artefact , when I click =Generate a random question= button , a random question is displayed which the user needs to work on .
+ Artefact also the basic operations of stacks and queues in te form of buttons.
+ Given artefact , when I click =check answer= button , the result is displayed with suitable explanation .

